#include <iostream>

using namespace std;

int main() {
    
   int firstNum = 0;
   int secondNum = 0;
   int min = 0;
   int max = 0;
   bool variable = true;
   
   cout << "Введите первое число: ";
   cin >> firstNum;
   cout << "\nВведите второе число: ";
   cin >> secondNum;
   
   max = (firstNum>secondNum) ? firstNum : secondNum;
   min = (firstNum<secondNum) ? firstNum : secondNum;
    
    cout << "\nМаксимальное: " << max;
    cout << "\nМинимально: " << min;
    
    
    return 0;
}
