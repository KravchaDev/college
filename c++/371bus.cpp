#include <iostream>

using namespace std;

int main() {
    
    int answer = 0;
    bool var = true;
    
    cout << "Введите номер остановки (От 1 до 3): ";
    
    do{
        cin >> answer;
        
        switch(answer)
        {
            case 1:
            cout << "\nОстановка Ермак: до Иркутска ехать 1 час 30 минут";
            break;
            case 2:
            cout << "\nОстановка Родина: до Иркутска ехать 1 час 20 минут";
            break;
            case 3:
            cout << "\nОстановка Автосервис: до Иркутска ехать 1 час 5 минут";
            break;
        default:
        cout << "Остановки под номером: " << answer << " не существует.";
        cout << "Сделайте правильный выбор (от 1 до 3): ";
        }
        if (answer >=1 && answer <=3)
        var = false;
    } while (var);
    
    return 0;
}
