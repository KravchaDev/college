#include <iostream>

using namespace std;

int main(){
    
    bool variable = true;
    
    cout << "Ваш заказ выдан!\n";
    cout << "Вы хотите сделать еще один заказ?\n";
    cout << "1 - Да || 0 - Нет\n";
    cin >> variable;
    
    variable == 1 ? cout << "\nСкажите, что бы вы хотели заказать?" : cout << "\nПриятного аппетита!";
    
    return 0;
}
