package main

import "fmt"

func main() {
  var name string
  var age int
  var weight float64
  name = "Ros"
  weight = 69.3
  age = 20

  printPersonalInfo(name, age, weight)
  isAdult := isAdult(age)
  fmt.Println(isAdult)
}

func printPersonalInfo (name string, age int, weight float64) {
  fmt.Printf("Имя:  %s\nВозраст: %d\nВес: %f\n", name, age, weight)
}

func isAdult (age int) bool {
  if age >= 18 {
    return true
  } 
  return false
}
