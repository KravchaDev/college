package main

import "fmt"

func main(){
    
    var x int16
    
    fmt.Println("Введите число: ")
    fmt.Scanln(&x)
    
    if x%2 == 0 {
        fmt.Println("Чётное")
    } else {
        fmt.Println("Нечётное")
    }
    
}
