package main

import "fmt"

func main() {
    var a [2]string
    a[0] = "Привет"
    a[1] = "Golang"
    fmt.Println(a[0], a[1])
    fmt.Println(a)

    primes := [6]int{29, 9, 2004, 7, 12}
    fmt.Println(primes)
}
