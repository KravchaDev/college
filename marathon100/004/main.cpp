#include <stdio.h>
#include <math.h>

using namespace std;

int main() {
    
    float a, v, s;
    printf("Введите длину ребра куба: ");
    scanf("%f", &a);
        v = pow(a,3);
        s = 6*pow(a,2);
    printf("Объем куба: %.2f", v);
    printf("\nПлощадь поверхности куба: %.2f", s);
}
