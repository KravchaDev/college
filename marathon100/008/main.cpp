#include <stdio.h>
#include <cmath>

int main(){

float a, b, sum, razn, proiz, chast;

printf ("Введите числа: ");
scanf("%f%f", &a, &b);

sum = abs(a)+abs(b);
razn = abs(a)-abs(b);
proiz = abs(a)*abs(b);
chast = abs(a)/abs(b);

printf ("\nСумма: %.1f", sum);
printf ("\nРазность: %.1f", razn);
printf ("\nПроизведение: %.1f", proiz);
printf ("\nЧастное: %.1f", chast);

return 0;
}
