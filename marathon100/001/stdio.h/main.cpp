#include <stdio.h>

using namespace std;

int main()
{
    float a, b, s, p;
    printf("Введите стороны прямоугольника: ");
    scanf("%f%f", &a, &b);
    s = a*b;
    p = 2*(a+b);
    printf("Площадь прямоугольника равна: %.0f", s);
    printf("\nПериметр прямоугольника равена: %.0f", p);
    return 0;
}
