#include <stdio.h>

using namespace std;

int main()
{
    float tc, tf;
    printf("Введите погоду в цельсиях: ");
    scanf("%f", &tc);
    tf = ((tc*9)/5+32);
    printf("Площадь прямоугольника равна: %.0f", tf);
    return 0;
}
