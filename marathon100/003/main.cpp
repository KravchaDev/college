#include <stdio.h>
#include <math.h>

using namespace std;

int main() {
    
    float d, l;
    printf("Введите диаметр окружности: ");
    scanf("%f", &d);
    l = 3.14*d;
    printf("Длина окружности в таком случае равна: %.2f", l);
    
}
