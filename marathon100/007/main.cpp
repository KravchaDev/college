#include <stdio.h>
#include <math.h>

int main() {
    float a, b, c, p;
    printf("Введите длину ваших катетов A и B: ");
    scanf("%f%f", &a, &b);
    c = pow((pow(a,2)+pow(b,2)), 0.5);
    p = a+b+c;
    printf("Гипотенуза : %.1f", c);
    printf("\nПериметр: %.1f", p);
}
